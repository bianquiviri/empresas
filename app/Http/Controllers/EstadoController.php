<?php

namespace App\Http\Controllers;

use App\Models\EmpresasDetalleModelo;
use App\Models\EstadosModelo;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // para mostrar el formulario
        return \view('estados.frmestado',\compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // para guardar un estado
        $NuevoEstado = new EstadosModelo();
        $NuevoEstado->id_pais = $request->idpais;
        $NuevoEstado->estado = $request->estado;
        $NuevoEstado->save();
        return \redirect('/dashboard/pais');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // mostra los estados de una empresa seleccionado

            $EmpresaDetalle = EmpresasDetalleModelo::where('id_empresa',$id)->first();

        $estados = EstadosModelo::where('id_pais',$EmpresaDetalle->id_pais)->get();

        return \view('estados.estadospais',\compact(['estados','id']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // para editar un estado
        $estado = EstadosModelo::find($id);
        return \view('estados.frmeditar',\compact('estado','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //  para guadar la actualizacion  del estado
        $id = $request->idestado;
        $Estado = EstadosModelo::find($id);
        $Estado->estado = $request->estado;
        $Estado->save();
        return \redirect('/dashboard/pais');

    }

    public function updateempresaestado(Request $request){
        // para actualizar los estado de una empresa
        $id = $request->id;
        $DetalleEmpresa = EmpresasDetalleModelo::find($id);
        $DetalleEmpresa->id_estado = $request->estados;
        $DetalleEmpresa->id_ciudad = $request->ciudades;
        $DetalleEmpresa->save();
        return \redirect('/dashboard/empresas')->with('message', 'Se ha modificado con exito el esatado y el pais ');

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function estadosselect(Request $request)
    {
        // Para mostrar un listado de los estados por pais
        $id_pais = $request->p;
        $estados = EstadosModelo::where ('id_pais',$id_pais)->orderBy('estado','asc')->get();
        return \view('estados.select',\compact('estados'));
    }

    static function getEstado($id_empresa){
        // para devolver el estado
        $Estado = EmpresasDetalleModelo::join('estados','empresa_detalle.id_estado','=','estados.id_estado')
            ->where('empresa_detalle.id_empresa',$id_empresa)
            ->select('estados.estado')->first ();
        if(!isset($Estado)){
            return   $Estado = 'No se encontro el estado';
        }else{
            return $Estado->estado;
        }
        return $Estado->estado;
    }
    public function getEstadoPais($id_pais)
    {
        // para devolver el estado
        $estados = EstadosModelo::where('id_pais', $id_pais)->orderBy('estado','asc')->get();
         return view('estados.listado', \compact('estados'));
    }
}
