<?php

namespace App\Http\Controllers;

use App\Models\CiudadesModelos;
use App\Models\EmpresasDetalleModelo;
use App\Models\EstadosModelo;
use Illuminate\Http\Request;

class CiudadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id_estado)
    {
        // para crea un nueva empresa
            return \view('ciudades.frmagregar',\compact('id_estado'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // para  guardar un nueva cuidad
        $NuevaCuidad = new CiudadesModelos();
        $NuevaCuidad->ciudad = $request->ciudad;
        $NuevaCuidad->id_estado = $request->idestado;
        $NuevaCuidad->save();
        return \redirect('/dashboard/pais');
    }


    public function edit($id)
    {
        // Para editar un ciudad
        $ciudad = CiudadesModelos::find($id);
        return \view('ciudades.frmeditar',\compact(['ciudad','id']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //   Para actualizar una ciudad
        $id = $request->idciudad;
        $ActualizarCiudad = CiudadesModelos::find($id);
        $ActualizarCiudad->ciudad =$request->ciudad;
        $ActualizarCiudad->save();
        return \redirect('dashboard/pais');

    }


    public function ciudadselect(Request $request){
        // para cargar un select con las cuidades
        $id_estado = $request->p;
        $ciudades = CiudadesModelos::where('id_estado',$id_estado)->orderBy('ciudad','asc')->get();
        return \view('ciudades.selectcuidades',\compact('ciudades'));
    }
    public function getCuidadesEstado($id_estado){
        $ciudades = CiudadesModelos::where('id_estado',$id_estado)->orderBy('ciudad','asc')->get();
        return \view('ciudades.listadoestado',\compact('ciudades'));
    }

    static function getCuidad($id_empresa){
        // para devolver el estado
          $Ciudades = EmpresasDetalleModelo::join('ciudades','empresa_detalle.id_ciudad','=','ciudades.id_ciudad')
            ->where('empresa_detalle.id_empresa',$id_empresa)
            ->select('ciudades.ciudad')->first ();
        if(!isset($Ciudades)){
            return   $Ciudades = 'Ciudad no encontrada';
        }else{
            return $Ciudades->ciudad;
        }
        return $Ciudades->ciudad;
    }



}
