<?php

namespace App\Http\Controllers;

use App\Models\EmpresasModelo;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ListaoEmpresa = new EmpresasController();
           return  $ListaoEmpresa->listadoempresas();
    }

    public function logout()
    {
        Auth::logout();
        return \redirect('/');
    }

    public function getUsuario()
    {
        if (Auth::check()) {
            return $usuario = Auth::user();
        } else {
            return $usuario = "null";
        }
    }

    public function editar($itemeditar, $id)
    {
        // para editar un item en

        switch ($itemeditar) {
            case 'NombreEmpresa': // Para editar el nombre
                $empresa = new EmpresasController();
                return $empresa->show('NombreEmpresa', $id);
                break;
            case 'DescripcionEmpresa': // Para editar el nombre
                $empresa = new EmpresasController();
                return $empresa->show('DescripcionEmpresa', $id);
                break;
            case 'pais': // para edita le pais
                $pais = new PaisesController();
                return $pais->show($id);
                break;
            case 'EstadoCiudad': // para edita le pais
                $estado = new EstadoController();
                return $estado->show($id);
            break;
            case 'tipologias': // para edita le pais
                $tipologia = new TipologiasController();
                return $tipologia->show($id);
                break;

            default:
                return \view('errors.404');
        }
    }
    public function usaurios(){
        // para listar los usuarios
             
    }

}
