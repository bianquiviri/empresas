<?php

namespace App\Http\Controllers;

use App\Models\EmpresasDetalleModelo;
use App\Models\EmpresasModelo;
use App\Models\PaisesModelo;
use Illuminate\Http\Request;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // para mostrar el listado de la empresas
        $active = 'Listado';
        $empresas =EmpresasModelo::paginate(10);

        /*
        $empresas = EmpresasDetalleModelo::join('empresas', 'empresa_detalle.id_empresa', '=', 'empresas.id_empresa')
            ->join('paises', 'empresa_detalle.id_pais', '=', 'paises.id_pais')
            ->join('estados', 'empresa_detalle.id_estado', '=', 'estados.id_estado')
            ->join('ciudades', 'empresa_detalle.id_ciudad', '=', 'ciudades.id_ciudad')
            ->join('tipologias', 'empresa_detalle.id_tipologia', '=', 'tipologias.id_tipologia')
            ->select('empresas.*', 'paises.*', 'estados.*', 'tipologias.*', 'ciudades.*')
            ->paginate(10);
    -*/
        return \view('empresas.listado', \compact('active', 'empresas'));
    }



      public function store(Request $request)
    {
        // Para guardar un empresa y devuelve el ultimo id en la tabla
        $UltimoId = EmpresasModelo::insertGetId(
            ['nombre_empresa' => $request->nombre, 'descripcion_empresa' => $request->descripcion]
        );
        // para obterner el ultimo id de la tabla empresas
        $NuevoDetalleEmpresa = new EmpresasDetalleModelo();
        $NuevoDetalleEmpresa->id_empresa = $UltimoId;
        $NuevoDetalleEmpresa->id_pais = $request->pais;
        $NuevoDetalleEmpresa->id_estado = $request->estados;
        $NuevoDetalleEmpresa->id_ciudad = $request->ciudades;
        $NuevoDetalleEmpresa->id_tipologia = $request->tipo;
        $NuevoDetalleEmpresa->save();
        // para mostrar los datos agregos
        return \redirect('/listadoempresas')->with('message', 'Se ha guardado una nueva empresa con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($EditarColumna, $id)
    {
        // para edita el nombre y el detalla de la empresa
        $empresa = EmpresasModelo::where('id_empresa', $id)->first();

        switch ($EditarColumna) {
            case 'NombreEmpresa': // para editar el nombre
                return \view('empresas.editar', \compact('empresa', 'EditarColumna'));
                break;
            case 'DescripcionEmpresa': // para editar la descripcion
                return \view('empresas.editar', \compact('empresa', 'EditarColumna'));
                break;
        }
    }


    public function update(Request $request)
    {
        // para actualizar la empresa
        $editarColummna = $request->Editarcolumna;
        $id = $request->id;
         switch ($editarColummna) {
            case 'NombreEmpresa':
                $Empresa = EmpresasModelo::find($id);
                $Empresa->nombre_empresa = $request->nombre;
                $Empresa->save();
                return \redirect('/dashboard/empresas')->with('message', 'Se ha relizado la modificaion con exito');
                break;
            case 'DescripcionEmpresa':
                $Empresa = EmpresasModelo::find($id);
                $Empresa->descripcion_empresa = $request->descripcion;
                $Empresa->save();
                return \redirect('/dashboard/empresas')->with('message', 'Se ha relizado la modificaion con exito');
                break;
            default:
                //    return \view('errors.404');
        }
    }


    public function listadoempresas()
    {
        /**
         * Para mostra el listado de empresas
         */
        /**

        $empresas = EmpresasDetalleModelo::join('empresas', 'empresa_detalle.id_empresa', '=', 'empresas.id_empresa')
            ->join('paises', 'empresa_detalle.id_pais', '=', 'paises.id_pais')
            ->join('estados', 'empresa_detalle.id_estado', '=', 'estados.id_estado')
            ->join('ciudades', 'empresa_detalle.id_ciudad', '=', 'ciudades.id_ciudad')
            ->join('tipologias', 'empresa_detalle.id_tipologia', '=', 'tipologias.id_tipologia')
            ->select('empresas.*', 'paises.*', 'estados.*', 'tipologias.*', 'ciudades.*')
            ->orderBy('paises.nombre_pais', 'asc')
            ->paginate(10);
*/

        $empresas = EmpresasModelo::orderby('nombre_empresa','asc')->paginate(10);

        $user = new HomeController();
        $usuario = $user->getUsuario();
        $paises = PaisesModelo::Orderby('nombre_pais', 'asc')->get();
        return view('empresas.listadoadmin', compact(['empresas', 'usuario', 'paises']));
    }

    public function buscar(Request $request){
        $texto = $request->textbuscar;
        $empresas = EmpresasModelo::where('nombre_empresa','like','%'.$texto.'%')-> orderby('nombre_empresa','asc')->paginate(10);
        $user = new HomeController();
        $usuario = $user->getUsuario();
        $paises = PaisesModelo::Orderby('nombre_pais', 'asc')->get();
        return view('empresas.listadoadmin', compact(['empresas', 'usuario', 'paises']));
    }
}
