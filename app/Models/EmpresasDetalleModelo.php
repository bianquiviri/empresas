<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresasDetalleModelo extends Model
{
    // Para guardar el detalle de las empresas
    public $primaryKey = 'id_detalleempresa';
    protected $table = 'empresa_detalle';
}
