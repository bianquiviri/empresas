<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmpresasModelo extends Model
{
    //tabla empresas
    public $primaryKey = 'id_empresa';
    protected $table = 'empresas';
}
