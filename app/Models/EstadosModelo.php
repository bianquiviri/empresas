<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstadosModelo extends Model
{
    // Tabla Estados
    public $primaryKey = 'id_estado';
    protected $table = 'estados';
}
