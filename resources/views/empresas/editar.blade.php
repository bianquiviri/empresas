<div>
     <form action="{{ url('/dashboard/empresaupdate') }}" method="post" class="form-horizontal-" role="form">
         {{csrf_field() }}
         <input type="hidden" id="id"  name="id" value="{{$empresa->id_empresa }}">
         <input type="hidden" id="Editarcolumna" name="Editarcolumna" value="{{ $EditarColumna }}">
     @if($EditarColumna =='NombreEmpresa')
        <div class="form-group">
            <label  for="">Nombre Empresa</label>
            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="" value="{{$empresa->nombre_empresa }}" required>
        </div>
         @endif
           @if($EditarColumna =='DescripcionEmpresa')
               <div class="form-group">
                   <label class="" for="">Descripcion de la Empresa</label>
                   <textarea id="descripcion" name="descripcion" cols="10" rows="10" class="form-control text-justify pre-scrollable"> {{$empresa->descripcion_empresa }} </textarea>
               </div>
        @endif
         <div class="pull-right">
             <button type="submit" class="btn btn-primary">Guardar</button>
          </div>
    </form>
</div>
<br>
<br>
