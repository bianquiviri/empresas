@extends('layouts.template')
@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <script type="text/javascript">
        $(document).ready(function () {
            iniciodiv();
            $('#pais').change(function () {
                if ($(this).val() === 'Selecione') {
                       iniciodiv();
                } else {
                    jsRotaPagina('{{ url('/estados')}}', 'divEstados', $(this).val());
                }
            });
            $('#tipo').change(function () {

                if ($(this).val() === 'Selecione') {
                    $(this).addClass('alert-danger');
                } else {
                    $(this).removeClass();
                    $(this).addClass('form-control');
                }
            });
            $('#bntGudar').click(function (event) {
                var flag = 'true';
                 if ($('#tipo').val() === 'Selecione') {
                    $('#tipo').addClass('alert-danger');
                      flag = 'false';
                }
                if ($('#pais').val() === 'Selecione') {
                    $('#pais').addClass('alert-danger');
                      flag = 'false';
                }
                if ($('#ciudades').val() === 'Selecione') {
                   $('#ciudades').addClass('alert-danger');
                   flag = 'false';
                }
                if ($('#ciudades').val() ==='') {
                    $('#ciudades').addClass('alert-danger');
                    flag = 'false';
                }
                if ($('#estados').val() === 'Selecione') {
                    $('#estados').addClass('alert-danger');
                    flag = 'false';
                }
                if ($('#estados').val()==='') {
                    $('#estados').addClass('alert-danger');
                    flag = 'false';
                }
                if ($('#descripcion').val() ==='') {
                    $('#labeldescripcion').addClass('text-danger');
                    flag = 'false';
                }

                  if ( flag === 'false'){
                        $('#error').show();
                  }
                  if( flag === 'true') {
                    $('#frmempresa').submit();
                }
                //
            });
        });
        function iniciodiv() {
            $('#error').hide();
            $('#divEstados').empty();
            $('#divCiudad').empty();
            $('#divEstados').html("<span class='tex text-danger  text-bold'>Seleccione el Pa&iacute;s</span>");
            $('#divCiudad').html("<span class='text text-danger text-bold'>Seleccione un Estado </span>");
        }


    </script>
       <div id="error" name="error">
           <div class="alert alert-danger">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
               <strong>Error !</strong> Valores requeridos
           </div>
       </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
            <div class="box box-default box-solid">
                <div class="box-header">
                    <i class="fa fc-agenda-view"></i>
                    <h3 class="box-title">REGISTRO DE EMPRESAS </h3>
                    <div class="box-tools">
                    </div>
                </div>
                <div class="box-body">
                    <p class="text-center text-black h3 ">INFORMACI&Oacute;N BASICA</p>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <form method="post" id="frmempresa" name="frmempresa" action="{{url('guardarempresa')}}">
                                {{csrf_field()  }}
                                <div class="well">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-static"> Nombre comercial Empresa</label>
                                                <input type="text" id="nombre" name="nombre" class="form-control"
                                                       >
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label for="tipo" class="form-control-static">Tipolog&iacute;a de la
                                                                                              empresa</label>
                                                <select id="tipo" name="tipo" class="form-control" required>
                                                    <option value="Selecione">Selecione la Tipolog&iacute;a</option>
                                                    @foreach($tipologias as $tipologia )
                                                        <option value="{{$tipologia->id_tipologia  }}"> {{$tipologia->nombre_tipologiaempresa }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="pais" class="form-control-static"> Pais</label>
                                                    <select id="pais" name="pais" class="form-control" required>
                                                        <option value="Selecione">Seleccione el Pais</option>
                                                        @foreach($paises as $pais)
                                                            <option value="{{ $pais->id_pais }}"> {{$pais->nombre_pais }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="estados" class="form-control-static">Estado</label>
                                                    <div id="divEstados" name="divEstados">
                                                        <select id="pais" name="pais" class="form-control"
                                                                disabled></select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="" class="form-control-static">Cuidad</label>
                                                    <div id="divCiudad" name="divCiudad">
                                                        <select id="pais" name="pais" class="form-control"
                                                                disabled></select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
                            <div class="form-group well">
                                <div class="form-group">
                                    <label for="comment" id="labeldescripcion" name="labeldescripcion"  >Descripci&oacute;n de la empresa</label>
                                    <textarea class="form-control" rows="5" id="descripcion" name="descripcion"
                                              style="height: 400px"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer pull-right">
                        <button type="button" class="btn  btn-toolbar" id="bntGudar" name="bntGudar">
                            <i class="fa fa-floppy-o"></i>
                            <span>Guardar</span>
                        </button>
                    </div>
                </div>
            </div>
            </form>



         </div>
    </div>

@endsection
