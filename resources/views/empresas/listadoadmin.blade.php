@extends('dashboard.template')
@section('content')
    <div class="box ">
        <div class="box-header">
            <i class="fa fa-building"></i>
            <h3 class="box-title">Empresas </h3>
            <div class="box-tools">
                <span class="badge">{{$empresas->total()  }}</span>
            </div>
        </div>
        <div class="box-body">
            <form class="form-inline" role="form" action="{{url('/dasborad/buscarempresa')  }}" method="post">
                {{csrf_field()  }}
                <div class="form-group">

                    <label class="sr-only" for="txtbuscar">Buscar</label>
                    <input type="text" class="form-control" id="textbuscar" name="textbuscar"
                           placeholder="Buscar en empresas" required autofocus>
                </div>
                <button type="submit" class="btn btn-default">Buscar</button>
                <a class="btn btn-default" href="{{url('dashboard/empresas') }}">
                    <i class="fa fa-refresh"></i>
                </a>
            </form>
            <hr>
            <div class="table-responsive">
                <table class="table table-hover table-responsive">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Decripci&oacute;n</th>
                        <th>Pais</th>
                        <th>Estado / Cuidad</th>
                        <th>Tipologia</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($empresas as $empresa)
                            <td style="width:15% ">{{$empresa->nombre_empresa  }}
                                <span class="pull-right">
                                  <a class="btn btn-default" data-toggle="modal" href="#modaleditar"
                                     onclick="jsRotaPagina('{{url('/dashboard/editar',['NombreEmpresa',$empresa->id_empresa])}}','divEditar','NoAplica')">
                                      <i class="fa fa-pencil-square"></i>
                                  </a>
                                    </i>
                                    </a>
                                  </span>
                            </td>
                            <td style="width: 40% "><p
                                        class="text-justify pre-scrollable"> {{$empresa->descripcion_empresa }}</p>
                                <span class="pull-right">
                                  <a class="btn btn-default" data-toggle="modal" href="#modaleditar"
                                     onclick="jsRotaPagina('{{url('/dashboard/editar',['DescripcionEmpresa',$empresa->id_empresa])}}','divEditar','NoAplica')">
                                      <i class="fa fa-pencil-square"></i>
                                </a>
                                  </span>
                            </td>
                            <td style="width: 15% "> {{ App\Http\Controllers\PaisesController::getNombrePais($empresa->id_empresa)}}</p>
                                <span class="pull-right">
                                  <a class="btn btn-default" data-toggle="modal" href="#modaleditar"
                                     onclick="jsRotaPagina('{{url('/dashboard/editar',['pais',$empresa->id_empresa])}}','divEditar','NoAplica')">
                                      <i class="fa fa-pencil-square"></i></a></span>
                            </td>
                            <td style="width: 15% "> {{App\Http\Controllers\EstadoController::getEstado($empresa->id_empresa)  }}
                                /
                                {{App\Http\Controllers\CiudadesController::getCuidad($empresa->id_empresa)  }}    {{ $empresa->ciudad }}
                                <span class="pull-right">
                                  <a class="btn btn-default" data-toggle="modal" href="#modaleditar"
                                     onclick="jsRotaPagina('{{url('/dashboard/editar',['EstadoCiudad',$empresa->id_empresa])}}','divEditar','NoAplica')">
                                      <i class="fa fa-pencil-square"></i></a></span>
                            </td>
                            <td style=" width: 10%">{{App\Http\Controllers\TipologiasController::getTipologia($empresa->id_empresa)  }}
                                <span class="pull-right">
                                  <a class="btn btn-default" data-toggle="modal" href="#modaleditar"
                                     onclick="jsRotaPagina('{{url('/dashboard/editar',['tipologias',$empresa->id_empresa])}}','divEditar','NoAplica')">
                                      <i class="fa fa-pencil-square"></i></a></span>
                            </td>
                    <tr>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="box-footer pull-right">
                {{ $empresas->appends(request()->all()) ->links() }}
            </div>
        </div>
    </div>
    <div class="modal fade" id="modaleditar">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Registros Empresas </h4>
                </div>
                <div class="modal-body">
                    <div id="divEditar" name="divEditar"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection