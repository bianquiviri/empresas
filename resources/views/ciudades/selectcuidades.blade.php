<script type="text/javascript">
    $(document).ready(function () {
        $('#ciudades').change(function(){
            if ($(this).val() === 'Selecione') {
                $(this).addClass('alert-danger');
            }
            else{
                $(this).removeClass();
                $(this).addClass('form-control');
            }
        });
    });
</script>
<select name="ciudades" id="ciudades" class="form-control alert-danger">
        <option value="Selecione">Seleccione La Ciudad</option>
    @foreach($ciudades as $ciudad)
        <option value="{{$ciudad->id_ciudad }}">{{$ciudad->ciudad }} </option>
    @endforeach
</select>
