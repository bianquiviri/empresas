<ul class="list-group ">
    @foreach($ciudades as $ciudad)
        <li class="list-group-item">{{ $ciudad->ciudad}}
            <a class="btn btn-xs btn-default  pull-right"
               onclick="jsRotaPagina('{{url('dashboard/editarciudad',$ciudad->id_ciudad )}}','divModal','NoAplica')"
               data-toggle="modal" href="#modal-id">Editar</a>
        </li>
    @endforeach
</ul>