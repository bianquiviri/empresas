<!DOCTYPE html>
<html lang = "es">
<head>
    <meta charset = "UTF-8">
    <title>{{ $page_title or "AulaSoft  Academica de Estudios  Tecnolog&iacute;cos  Infom&aacute;ticos" }}</title>
    <meta content = 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
          name = 'viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href = "{{ asset("/bootstrap-3.3.7/css/bootstrap.css") }}" rel = "stylesheet"
          type = "text/css"/>
    <!-- tema bootstrap -->
    <!-- Para mejorar los espacios  -->
    <!-- Bootstrap 3.3.2 JS -->
    <script src = "{{ asset ("/bootstrap-3.3.7/js/bootstrap.js") }}"
            type = "text/javascript"></script>
    <script src = "{{ asset ('emprempresas.js')}}" type = "text/javascript"></script>

    <script src = "{{ asset ("/js/jquery.blockUI.js") }}" type = "text/javascript"></script>

    <!-- para procesar los formularios desde ajax -->

    <script src = "{{ asset ("/js/jquery.form.js") }}" type = "text/javascript"></script>
    <!-- para encripta a md5 -->

    <script src = "{{ asset ("/js/jquery.md5.js") }}" type = "text/javascript"></script>

    <!-- Bootstrap date picker  JS -->
    <script src="{{ asset ("/bower_components/admin-lte/plugins/datepicker/bootstrap-datepicker.js") }}"
            type="text/javascript" ></script >

    <link href="{{ asset("/bower_components/admin-lte/plugins/datepicker/datepicker3.css")}}" rel="stylesheet"
          type="text/css" />
    <!-- Bootstrap date picker  JS -->
    <script src="{{ asset ("/bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.js") }}"
            type="text/javascript" ></script >

    <link href="{{ asset("/bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.css")}}" rel="stylesheet"
          type="text/css" />


    <link href = "https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"
          rel = "stylesheet"
          type = "text/css"/>
    <link href = "http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css"
          rel = "stylesheet" type = "text/css"/>
    <!-- Theme style -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css"
          rel="stylesheet" type="text/css"/>

    <!--	<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.css" rel="stylesheet"
              type="text/css"/>-->
</head>
