<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Ejemplo de desarrollo para Develoop">
    <meta name="author" content="Jose Antonio Bianco Paredes">
    <link rel="icon" href="{{asset('imagenes/logo.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $page_title or "Registros de Empresas" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
          name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <!-- Para mejorar los espacios  -->
    <!-- Para  la libreria jquery -->
    <script src="{{ asset ("/js/jquery-2.2.3.min.js") }}" type="text/javascript"></script>
    <!-- para bootstrap -->
    <script src="{{ asset ("/bootstrap-3.3.7/js/bootstrap.js") }}" type="text/javascript"></script>

    <script src="{{ asset ("/js/empresas.js") }}" type="text/javascript"></script>
    <!-- blockui cuando se ejecuta un ajax -->

    <script src="{{ asset ("/js/jquery.blockUI.js") }}" type="text/javascript"></script>
    <!-- para procesar los formularios desde ajax -->
    <script src="{{ asset ("/js/jquery.form.js") }}" type="text/javascript"></script>
    <!-- para encripta a md5 -->
    <script src="{{ asset ("/js/jquery.md5.js") }}" type="text/javascript"></script>
    <!-- para validar los formularios -->
    <script src="{{ asset ("/js/jquery.validate.js") }}" type="text/javascript"></script>


    <!--	<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.css" rel="stylesheet"
              type="text/css"/>-->

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="{{ asset("/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ asset("/bower_components/AdminLTE/dist/css/AdminLTE.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link href="{{ asset("/bower_components/AdminLTE/dist/css/skins/skin-blue.min.css")}}" rel="stylesheet" type="text/css" />
<!-- datatables -->
    <link href="{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}" rel="stylesheet" type="text/css" />
    <script src="{{ asset ("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.js") }}" type="text/javascript"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
</head>
<body style="padding-top: 70px"><!-- Page Content -->
<script type="text/javascript">
    $(document).ready(function(){
        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
        activo('{{$active or null}}');
        function activo(active) {
            var active;
            $('#' + active + '').addClass('active');
        }
    });
</script>
<nav class="navbar-collapse  navbar-inverse  navbar-fixed-top " role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand"  href="{{ url('/empresas') }}">Inicio</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
            <li  id="empresa" name="empresa" ><a href="{{ url('/empresas') }}">Empresa</a></li>
            <li  id="empresa" name="empresa" ><a href="{{ url('/listadoempresas') }}">Empresas Agregadas</a></li>


        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/register') }}">Registro</a></li>
       <li><a href="{{url('/login') }}">Iniciar Sessi&oacute;n</a></li>
  </ul>
    </div><!-- /.navbar-collapse -->
</nav>
                               <!-- Page Content -->
<div class="container-fluid">

    <!-- Page Heading/Breadcrumbs -->
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}">Inicio</a>
                </li>
                <li class="active">{{ $breadcrumbs or null}} </li>
            </ol>
        </div>
    </div>

    <!-- Page Heading/Breadcrumbs -->
    <!-- Content Row -->
    <div class="container-fluid" style="min-height: 500px">
                 @yield('content')
        </div>
    <!-- /.row -->
    <hr class="alert-info" style="height: 20px">
@include('layouts.footer')
<!-- Footer -->
    <!-- REQUIRED JS SCRIPTS -->
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
          Both of these plugins are recommended to enhance the
          user experience -->
</div>
</body>
</html>