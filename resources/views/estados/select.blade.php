<script type="text/javascript">
    $(document).ready(function () {
            $('#estados').change(function(){
                if ($(this).val() === 'Selecione') {
                    $(this).addClass('alert-danger');
                }
                else{
                    $(this).removeClass();
                    $(this).addClass('form-control');
                    jsRotaPagina('{{ url('/ciudades')}}','divCiudad',$(this).val());
                }
            });
    });
</script>
<select name="estados" id="estados" class="form-control alert-danger">
        <option value="Selecione">Seleccione el Estado</option>
                @foreach($estados as $estado)
        <option value="{{$estado->id_estado }}">{{$estado->estado }} </option>
    @endforeach
</select>
