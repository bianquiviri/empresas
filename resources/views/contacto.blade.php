@extends('layouts.template')
@section('content')
    <div class="box box-info">
        <div class="box-header">
            <h3 class="box-title">Enviar un correo</h3>
        </div><!-- /.box-header -->
        <div class="box-body">

                <form method="post" class="form-horizontal" name="frmenviar" id="frmenviar" action=""
                      onsubmit="jsProcesarFormulario('frmenviar','{{url ('/admin/enviartodos.html') }}','divfrmEnvio')">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="idcurso" id="idcurso" value="{{ $idcurso or null}}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                        <span>Este Correo sera enviado a <span class="badge">{{ $cantidad or null}}</span>
                        Alumnos</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Mensaje</label>
                            <div class="col-sm-10">
                        <textarea id="mensaje" name="mensaje" class="form-control" cols="10"
                                  rows="10"> </textarea>
                            </div>
                        </div>
                        <div class="box-footer pull-right">
                            <button type="button"
                                    onclick="jsProcesarFormulario('frmenviar','{{url ('/admin/enviartodos.html') }}','divListadoAlumno')"
                                    class="btn btn-default" data-dismiss="modal">
                                <span class="glyphicon glyphicon-send"></span>
                                Enviar Mensaje
                            </button>
                        </div><!-- /.box-footer -->
                    </div>
                </form>

        </div>
    </div>
@endsection