<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'password' => 'Passwords tiene que tener al menos 6 caracteres',
    'reset' => 'Su password ha sido reseteado !',
    'sent' =>  ' Te hemo enviad un mensaje para que puedas restear  tu clave',
    'token' => 'This password reset token is invalid.',
    'user' => "No hemos podio encontrar ese correo .",
];
